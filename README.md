# Scrapinghub (now Zyte) coding test #

Ok, to clarify: I just love to do tests from time to time. To check my own skills, to borrow some ideas for students' tests, etc. So here's the one I did for this company. It's very basic but I do find it somewhat amusing because you can have the idea of a basic Erlang module, testing it with eunit, and a very simple Makefile config.

`make` or `make test`, that's it.

