-module(proxy_manager).

-export([start/0]).
-export([set_proxies/1]).
-export([list_proxies/0]).
-export([get_proxy/1]).
-export([ban_proxy/2]).
-export([unban_proxy/2]).
-export([banned_proxies/0]).
-export([banned_proxies/1]).

-export([loop/2]).

-type proxy() :: term().
-type netloc() :: term().

-type bans() :: #{netloc() => [proxy()]}.

-define(LOOP_ID, proxy_manager_loop_pid).


%% Interface.

-spec start() -> {ok, pid()}.
start() ->
	Pid = spawn(?MODULE, loop, [[], #{}]),
	register(?LOOP_ID, Pid),
	{ok, Pid}.

-spec set_proxies([proxy()]) -> ok.
%% @doc
%% Initialize subsystem state with given proxies
%% @end
set_proxies(Proxies) ->
	whereis(?LOOP_ID) ! {set_proxies, Proxies},
	ok.

-spec list_proxies() -> [proxy()].
%% @doc
%% List proxies
%% @end
list_proxies() ->
	whereis(?LOOP_ID) ! {get_proxies, self()},
	receive
		Proxies -> Proxies
	end.

-spec get_proxy(netloc()) -> {ok, proxy()} | {error, no_proxies}.
%% @doc
%% Return clean proxy for given netloc or error, if no proxies available
%% @end
get_proxy(Netloc) ->
	whereis(?LOOP_ID) ! {get_proxy, Netloc, self()},
	receive
		{error, no_proxies} -> {error, no_proxies};
		{ok, Proxy} -> {ok, Proxy}
	end.

-spec ban_proxy(proxy(), netloc()) -> ok.
%% @doc
%% Ban proxy for netloc
%% @end
ban_proxy(Proxy, Netloc) ->
	whereis(?LOOP_ID) ! {ban_proxy, Netloc, Proxy},
	ok.

-spec unban_proxy(proxy(), netloc()) -> ok.
%% @doc
%% Unban proxy for netloc
%% @end
unban_proxy(Proxy, Netloc) ->
	whereis(?LOOP_ID) ! {unban_proxy, Netloc, Proxy},
	ok.

-spec banned_proxies() -> [{netloc(), [proxy()]}].
%% @doc
%% List banned proxies per netloc
%% @end
banned_proxies() ->
	whereis(?LOOP_ID) ! {banned_proxies, self()},
	receive
		List -> List
	end.

-spec banned_proxies(netloc()) -> [proxy()].
%% @doc
%% List banned proxies for netloc
%% @end
banned_proxies(Netloc) ->
	whereis(?LOOP_ID) ! {banned_proxies, Netloc, self()},
	receive
		List -> List
	end.


%% Internal loop.

-spec loop([proxy()], bans()) -> [proxy()].
loop(Proxies, Bans) ->
	receive
		{set_proxies, NewProxies} ->
			loop(NewProxies, Bans);

		{get_proxies, Sender} ->
			Sender ! Proxies,
			loop(Proxies, Bans);

		{get_proxy, Netloc, Sender} ->
			Sender ! choose_proxy(Netloc, Proxies, Bans),
			loop(Proxies, Bans);

		{ban_proxy, Netloc, Proxy} ->
			NewBans = case lists:member(Proxy, Proxies) of
				true -> Bans#{Netloc => [Proxy | maps:get(Netloc, Bans, [])]};
				false -> Bans
			end,
			loop(Proxies, NewBans);

		{unban_proxy, Netloc, Proxy} ->
			Banned = maps:get(Netloc, Bans, []),
			loop(Proxies, Bans#{Netloc => Banned -- [Proxy]});

		{banned_proxies, Sender} ->
			Sender ! maps:to_list(Bans),
			loop(Proxies, Bans);

		{banned_proxies, Netloc, Sender} ->
			Sender ! maps:get(Netloc, Bans, []),
			loop(Proxies, Bans)
	end.

-spec choose_proxy(netloc(), [proxy()], bans()) -> {ok, proxy()} | {error, no_proxies}.
choose_proxy(_Netloc, _Proxies = [], _Bans) ->
	{error, no_proxies};
choose_proxy(Netloc, Proxies, Bans) ->
	Banned = maps:get(Netloc, Bans, []),
	case Proxies--Banned of
		[] -> {error, no_proxies};
		Unbanned -> {ok, lists:nth(rand:uniform(length(Unbanned)), Unbanned)}
	end.

