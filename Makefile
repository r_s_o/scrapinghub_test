BEAMS = $(patsubst src/%.erl,ebin/%.beam,$(wildcard src/*.erl))
TEST_BEAMS = $(patsubst tests/%.erl,tests/%.beam,$(wildcard tests/*.erl))


all: $(BEAMS)


ebin/%.beam: src/%.erl
	@erlc -o ebin $<


tests/%.beam: tests/*.erl
	@erlc -o ebin $<


test: all $(TEST_BEAMS)
	@erl -noshell -noinput -pa ebin -pa tests -eval "case eunit:test(proxy_manager, [verbose]) of error -> halt(1); _ -> halt(0) end"
